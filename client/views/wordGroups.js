import {JetView} from 'webix-jet';

class WordGroups extends JetView {
    config() {
        const _ = this.app.getService('locale')._;
        const user = this.app.getService('user').getUser();
        const groupsList = {
            view: 'list',
            localId: 'groupsList',
            template: `${_('Name:')}#name#<br>${_('Creation date:')}#creationDate#<br>${_('Words amount:')}#wordsAmount#`,
            url: `/api/groups?userId=${user._id}`,
            width: 500,
            tooltip: _('Click twice to add words to group'),
            type: {
                height: 90
            },
            on: {
                onItemDblClick: (id) => {
                    this.app.callEvent('unselectListItem');
                    this.app.show(`/mainPage/addWord?id=${id}`);
                }
            }
        };

        const groupForm = {
            view: 'form',
            localId: 'groupForm',
            elementsConfig: {
                labelWidth: 150,
                labelAlign: 'left'
            },
            elements: [
                {view: 'text', name: 'name', label: _('Group name'), tooltip: _("This field can't be empty"), invalidMessage: _("Can't be empty")},
                {},
                {view: 'button',
                    value: _('Create group'),
                    click: () => {
                        const values = this.$$('groupForm').getValues();
                        if (this.$$('groupForm').validate()) {
                            values.creationDate = new Date();
                            values.wordsAmount = 0;
                            values.userId = user._id;
                            webix.ajax().post('/api/createGroup', values).then((obj) => {
                                this.$$('groupsList').add(obj.json());
                                this.$$('groupForm').clear();
                            }).catch(() => {
                                webix.message(_('Server error occured. Please try again later.'), 'error');
                            });
                        }
                    }}
            ],
            rules: {
                $all: webix.rules.isNotEmpty
            }
        };

        return {
            cols: [
                groupsList,
                groupForm
            ]
        };
    }
}

export default WordGroups;
