import {JetView} from 'webix-jet';

class LoginWin extends JetView {
    config() {
        const _ = this.app.getService('locale')._;
        return {
            view: 'window',
            type: 'clean',
            head: _('Login'),
            position: 'center',
            height: 350,
            width: 600,
            body: {
                view: 'form',
                localId: 'logForm',
                elementsConfig: {
                    labelWidth: 200,
                    labelAlign: 'left'
                },
                elements: [
                    {view: 'text', label: _('E-mail Address'), name: 'email', tooltip: _("This field can't be empty")},
                    {view: 'text', label: _('Password'), type: 'password', name: 'password', tooltip: _("This field can't be empty"), invalidMessage: _("Can't be empty")},
                    {cols: [
                        {view: 'button', value: _('Login'), inputWidth: 80, click: () => { this.doLogin(); }, hotkey: 'enter'},
                        {template: `<span class='passRec'>${_('Forgot your password')}?</span>`,
                            css: 'passForgot',
                            onClick: {
                                passRec: () => {
                                    this.app.callEvent('passRec');
                                }
                            }}
                    ]}
                ]
            }
        };
    }
    showWin() {
        this.getRoot().show();
    }
    hideWin() {
        this.getRoot().hide();
    }
    doLogin() {
        const _ = this.app.getService('locale')._;
        const user = this.app.getService('user');
        const form = this.$$('logForm');
        if (form.validate()) {
            const data = form.getValues();
            user.login(data).catch((err) => {
                if (!err.status) {
                    webix.html.removeCss(form.$view, 'invalid_login');
                    form.elements.password.focus();
                    webix.delay(() => {
                        webix.html.addCss(form.$view, 'invalid_login');
                        webix.message(_('Incorrect e-mail or password'), 'info');
                    });
                }
                else {
                    webix.message(_('Serever error occured. Please try again later.'), 'error');
                }
            });
        }
    }
}

export default LoginWin;
