import {JetView} from 'webix-jet';

class SettingsPage extends JetView {
    config() {
        const langs = this.app.getService('locale');
        const toggleLanguage = () => {
            const value = this.$$('langButton').getValue();
            langs.setLang(value);
        };

        const langButton = {
            view: 'segmented',
            localId: 'langButton',
            value: langs.getLang(),
            multiview: true,
            options: [
                {id: 'en', value: 'English'},
                {id: 'ru', value: 'Russian'}
            ],
            click: () => toggleLanguage()
        };
        return {
            rows: [
                langButton,
                {}
            ]
        };
    }
}

export default SettingsPage;
