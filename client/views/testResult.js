import {JetView} from 'webix-jet';

class TestResultsPage extends JetView {
    config() {
        const _ = this.app.getService('locale')._;
        const user = this.app.getService('user').getUser();
        let testNumber = 1;
        const resultTable = {
            view: 'datatable',
            localId: 'resultTable',
            url: `/api/getUsersResults?userId=${user._id}`,
            scheme: {
                $init: (obj) => {
                    const item = obj;
                    item.testNumber = testNumber;
                    testNumber++;
                    return item;
                }
            },
            columns: [
                {id: 'testNumber', header: '#'},
                {id: 'testDate', header: _('Date'), fillspace: true},
                {id: 'score', header: _('Score'), fillspace: true},
                {id: 'correctUnswers', header: _('Correct Unswers'), width: 200},
                {id: 'incorrectUnswers', header: _('Incorrect Unswers'), width: 200}
            ]
        };
        return resultTable;
    }
}

export default TestResultsPage;
