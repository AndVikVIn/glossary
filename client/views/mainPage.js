import {JetView} from 'webix-jet';
import '../styles/app.css';


class mainPage extends JetView {
    config() {
        const _ = this.app.getService('locale')._;
        const user = this.app.getService('user').getUser();
        const toolbar = {
            view: 'toolbar',
            localId: 'toolbar',
            cols: [
                {view: 'label',
                    label: _('Glossary'),
                    css: 'headerLabel',
                    click: () => {
                        this.show('/mainPage/wordGroups');
                    }},
                {view: 'label', label: `${_('Hello')} ${user.name}`, css: 'headerLabel'},
                {view: 'button',
                    value: _('Logout'),
                    width: 100,
                    click: () => {
                        this.app.getService('user').logout().then(() => { this.app.show('afterLogout'); });
                    }}
            ]
        };

        const navigationList = {
            view: 'list',
            localId: 'navigationList',
            select: true,
            width: 200,
            template: '#value#',
            data: [
                {id: 'wordGroups', value: _('Word groups')},
                {id: 'tests', value: _('Tests')},
                {id: 'testResult', value: _('Test results')},
                {id: 'settings', value: _('Settings')}
            ],
            on: {
                onAfterSelect: (id) => {
                    this.show(`/mainPage/${id}`);
                }
            }
        };

        return {
            rows: [
                toolbar,
                {cols: [
                    navigationList,
                    {$subview: true}
                ]}
            ]
        };
    }
    init() {
        this.on(this.app, 'unselectListItem', () => {
            this.$$('navigationList').unselectAll();
        });
        this.on(this.app, 'selectListItem', (page) => {
            this.$$('navigationList').select(page);
        });
    }
}

export default mainPage;
