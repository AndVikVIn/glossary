import {JetView} from 'webix-jet';
import '../styles/app.css';

class TestPage extends JetView {
    config() {
        const user = this.app.getService('user').getUser();
        const _ = this.app.getService('locale')._;
        const groupChoose = {
            view: 'richselect',
            localId: 'groupChoose',
            label: _('Group'),
            options: {
                body: {
                    view: 'list',
                    url: `/api/groups?userId=${user._id}`,
                    template: '#name#'
                }
            }
        };

        const testArr = [];
        const testCarousel = {
            view: 'carousel',
            localId: 'testCarousel',
            navigation: {
                type: 'side',
                items: false
            },
            cols: testArr
        };

        let score = 0;
        let correctUnswers = 0;
        let incorrectUnswers = 0;
        const scoreTemp = {
            localId: 'score',
            template: `${_('Your score is:')} ${score}`,
            hidden: true,
            height: 40
        };

        const getRandomTask = (groupId) => {
            this.$$('endButton').hide();
            score = 0;
            webix.ui(testCarousel, this.$$('testCarousel'));
            this.$$('score').show();
            webix.ui({localId: 'score', template: `${_('Your score is:')} ${score}`, height: 40}, this.$$('score'));
            webix.ajax().get('/api/getRandome', {groupId}).then((obj) => {
                const items = obj.json();
                function getRandom() {
                    return Math.random() - 0.5;
                }

                const unswerCheck = (buttonId, i) => {
                    const parent = this.$$(buttonId).getParentView();
                    if (parent.config.localId === (`question${(items.length - 1)}`)) {
                        this.$$('endButton').show();
                    }
                    if (this.$$(buttonId).getValue() === items[i][0].translation) {
                        this.$$(buttonId).define('css', 'correctUnswerButton');
                        correctUnswers++;
                        if (items[i][0].type === 'Noun' || items[i][0].type === 'Verb') {
                            score += 2;
                        }
                        else { score++; }
                        this.$$(`question${i}`).disable();
                        webix.ui({localId: 'score', template: `${_('Your score is:')} ${score}`, height: 40}, this.$$('score'));
                        this.$$('testCarousel').showNext();
                    }
                    else {
                        this.$$(buttonId).define('css', 'incorrectUnswerButton');
                        incorrectUnswers++;
                        this.$$(`question${i}`).disable();
                        this.$$('testCarousel').showNext();
                    }
                };

                for (let i = 0; i < items.length; i++) {
                    if (items[i].length === 4) {
                        const indexes = [0, 1, 2, 3];
                        const sortedIndexes = indexes.sort(getRandom);
                        const question = {
                            localId: `question${i}`,
                            rows: [
                                {template: `${_('Translation of')} ${items[i][0].name} ${_('is:')}`},
                                {view: 'button', click: (id) => { unswerCheck(id, i); }, value: items[i][sortedIndexes[0]].translation},
                                {view: 'button', click: (id) => { unswerCheck(id, i); }, value: items[i][sortedIndexes[1]].translation},
                                {view: 'button', click: (id) => { unswerCheck(id, i); }, value: items[i][sortedIndexes[2]].translation},
                                {view: 'button', click: (id) => { unswerCheck(id, i); }, value: items[i][sortedIndexes[3]].translation}
                            ]
                        };
                        this.$$('testCarousel').addView(question);
                    }
                }
            }).catch(() => {
                webix.message(_('Server error occured. Please try again later.'), 'error');
            });
        };

        const endBut = {
            view: 'button',
            localId: 'endButton',
            hidden: true,
            value: _('End test'),
            click: () => {
                this.app.callEvent('selectListItem', ['testResult']);
                webix.ajax().post('/api/addNewResult', {userId: user._id, score, correctUnswers, incorrectUnswers, testDate: new Date()})
                    .catch(() => {
                        webix.message(_('Server error occured. Please try again later.'), 'error');
                    });
            }
        };

        const startBut = {
            view: 'button',
            value: _('Start test'),
            click: () => {
                const groupId = this.$$('groupChoose').getValue();
                getRandomTask(groupId);
            }
        };
        return {
            rows: [
                groupChoose,
                startBut,
                scoreTemp,
                testCarousel,
                endBut
            ]
        };
    }
}

export default TestPage;
