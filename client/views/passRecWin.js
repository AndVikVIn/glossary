import {JetView} from 'webix-jet';

class PassRecWin extends JetView {
    config() {
        const _ = this.app.getService('locale')._;
        return {
            view: 'window',
            type: 'clean',
            head: _('Password recovery'),
            position: 'center',
            height: 250,
            width: 600,
            body: {
                view: 'form',
                localId: 'passRecForm',
                elementsConfig: {
                    labelWidth: 150,
                    labelAlign: 'left'
                },
                elements: [
                    {view: 'text', localId: 'email', label: _('E-mail Address'), name: 'email', tooltip: "This field can't be empty"},
                    {view: 'button', value: _('Send password reset link'), inputWidth: 250}
                ],
                rules: {
                    email: webix.rules.isNotEmpty
                }
            }
        };
    }
    showWin() {
        this.getRoot().show();
    }
    hideWin() {
        this.getRoot().hide();
    }
}

export default PassRecWin;
