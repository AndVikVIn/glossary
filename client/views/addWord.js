import {JetView} from 'webix-jet';

class AddWordPage extends JetView {
    config() {
        const _ = this.app.getService('locale')._;
        const groupTable = {
            view: 'datatable',
            localId: 'groupTable',
            fixedRowHeight: false,
            rowLineHeight: 25,
            rowHeight: 30,
            columns: [
                {id: 'name', header: [_('Word'), {content: 'textFilter'}], sort: 'string', fillspace: true},
                {id: 'translation', header: [_('Translation'), {content: 'textFilter'}], sort: 'string', fillspace: true},
                {id: 'type', header: [_('Type'), {content: 'selectFilter'}], sort: 'string', fillspace: true}
            ]
        };

        const wordForm = {
            view: 'form',
            localId: 'wordForm',
            elementsConfig: {
                labelWidth: 150,
                labelAlign: 'left'
            },
            elements: [
                {view: 'text', name: 'name', label: _('Word'), tooltip: _("This field can't be empty"), invalidMessage: _("Can't be empty")},
                {view: 'text', name: 'translation', label: _('Translation'), tooltip: _("This field can't be empty"), invalidMessage: _("Can't be empty")},
                {view: 'select',
                    name: 'type',
                    label: _('Type'),
                    options: [
                        {id: 'Noun', value: _('Noun')},
                        {id: 'Adjective', value: _('Adjective')},
                        {id: 'Pronoun', value: _('Pronoun')},
                        {id: 'Numeral', value: _('Numeral')},
                        {id: 'Verb', value: _('Verb')},
                        {id: 'Adverb', value: _('Adverb')}
                    ]},
                {view: 'button',
                    value: _('Create translation'),
                    click: () => {
                        const values = this.$$('wordForm').getValues();
                        const groupId = this.getParam('id');
                        if (this.$$('wordForm').validate()) {
                            values.groupId = groupId;
                            webix.ajax().post('/api/createWord', values).then((obj) => {
                                this.$$('groupTable').add(obj.json());
                                this.$$('wordForm').clear();
                                const amount = this.$$('groupTable').count();
                                return webix.ajax().post('/api/changeGroup', {groupId, wordsAmount: amount});
                            })
                                .catch(() => {
                                    webix.message(_('Server error occured. Please try again later.'), 'error');
                                });
                        }
                    }}
            ],
            rules: {
                $all: webix.rules.isNotEmpty
            }
        };

        const exportToExcel = {
            view: 'button',
            value: _('Export to excel'),
            click: () => {
                webix.toExcel(this.$$('groupTable'));
            }
        };

        return {
            rows: [
                wordForm,
                groupTable,
                exportToExcel
            ]
        };
    }
    init() {
        const _ = this.app.getService('locale')._;
        const groupId = this.getParam('id');
        webix.ajax().get('/api/words', {groupId}).then((obj) => {
            this.$$('groupTable').parse(obj.json());
        }).catch(() => {
            webix.message(_('Server error occured. Please try again later.'), 'error');
        });
    }
}

export default AddWordPage;
