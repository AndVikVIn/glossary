import {JetView} from 'webix-jet';

class RegisterWin extends JetView {
    config() {
        const _ = this.app.getService('locale')._;
        const emailCheck = () => {
            const value = this.$$('email').getValue();
            webix.ajax().get('/api/checkEmail', {email: value}).then((data) => {
                if (data.text()) {
                    this.$$('email').check = true;
                }
                else {
                    this.$$('email').check = false;
                }
            }).catch(() => {
                webix.message(_('Server error. Please try again later.'), 'error');
            });
        };

        return {
            view: 'window',
            type: 'clean',
            head: 'Register',
            position: 'center',
            height: 450,
            width: 600,
            body: {
                view: 'form',
                localId: 'regForm',
                elementsConfig: {
                    labelWidth: 150,
                    labelAlign: 'left'
                },
                elements: [
                    {view: 'text', label: _('Name'), name: 'name', tooltip: _("This field can't be empty"), invalidMessage: _("Can't be empty")},
                    {view: 'text', localId: 'email', label: _('E-mail Address'), name: 'email', tooltip: _("This field can't be empty"), on: {onBlur: emailCheck}},
                    {view: 'text', localId: 'password', label: _('Password'), type: 'password', name: 'password', tooltip: _("This field can't be empty"), invalidMessage: _("Can't be empty")},
                    {view: 'text', localId: 'confirmPass', label: _('Confirm Password'), type: 'password', name: 'confirmPass', tooltip: _("This field can't be empty"), invalidMessage: _("Can't be empty")},
                    {view: 'button',
                        value: 'Register',
                        inputWidth: 80,
                        click: () => {
                            const values = this.$$('regForm').getValues();
                            emailCheck();
                            if (this.$$('regForm').validate()) {
                                if (values.password === values.confirmPass) {
                                    values.creationDate = new Date();
                                    webix.ajax().post('/api/newUser', values).then(() => {
                                        const user = this.app.getService('user');
                                        user.login(values);
                                    }).catch(() => {
                                        webix.message(_('Server error occured. Please try again later.'), 'error');
                                    });
                                }
                                else {
                                    webix.message(_('Incorrect password!'), 'error');
                                }
                            }
                        }}
                ],
                rules: {
                    name: webix.rules.isNotEmpty,
                    email: (value) => {
                        if (value.length === 0) {
                            this.$$('email').define('invalidMessage', _("Can't be empty"));
                            return false;
                        }
                        if (!webix.rules.isEmail(value)) {
                            this.$$('email').define('invalidMessage', _('Incorrect E-mail'));
                            return false;
                        }
                        if (this.$$('email').check) {
                            this.$$('email').define('invalidMessage', _('This E-mail already have been taken'));
                            return false;
                        }
                        return true;
                    },
                    password: webix.rules.isNotEmpty,
                    confirmPass: (value) => {
                        if (value.length === 0) {
                            this.$$('confirmPass').define('invalidMessage', _("Can't be empty"));
                            return false;
                        }
                        const confirmPass = this.$$('confirmPass').getValue();
                        const password = this.$$('password').getValue();
                        if (password !== confirmPass) {
                            this.$$('confirmPass').define('invalidMessage', _('Incorrect password confirmation'));
                            return false;
                        }
                        return true;
                    }
                }
            }
        };
    }
    showWin() {
        this.getRoot().show();
    }
    hideWin() {
        this.getRoot().hide();
    }
}

export default RegisterWin;
