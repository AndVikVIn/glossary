import {JetView} from 'webix-jet';
import '../styles/app.css';
import RegisterWin from './registerWin';
import LoginWin from './loginWin';
import PassRecWin from './passRecWin';

class LoginPage extends JetView {
    config() {
        const _ = this.app.getService('locale')._;
        const toolbar = {
            view: 'toolbar',
            localId: 'toolbar',
            cols: [
                {view: 'label', label: _('Glossary'), css: 'headerLabel'},
                {view: 'button',
                    value: _('Login'),
                    width: 100,
                    click: () => {
                        this.registerWin.hideWin();
                        this.loginWin.showWin();
                    }},
                {view: 'button',
                    value: _('Register'),
                    width: 100,
                    click: () => {
                        this.loginWin.hideWin();
                        this.registerWin.showWin();
                    }}
            ]
        };
        const welcome = {
            template: _('Thank you for using our glossary!'),
            css: 'welcomeMessage'
        };
        return {
            rows: [
                toolbar,
                welcome
            ]
        };
    }
    init() {
        this.registerWin = this.ui(RegisterWin);
        this.loginWin = this.ui(LoginWin);
        this.passRecWin = this.ui(PassRecWin);
        this.on(this.app, 'passRec', () => {
            this.loginWin.hideWin();
            this.passRecWin.showWin();
        });
    }
}

export default LoginPage;
