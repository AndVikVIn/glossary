const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    name: String,
    password: String,
    email: String
});

module.exports = userSchema;
