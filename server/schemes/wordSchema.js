const mongoose = require('mongoose');

const wordSchema = mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    name: String,
    translation: String,
    type: String,
    groupId: mongoose.Schema.Types.ObjectId
});

module.exports = wordSchema;
