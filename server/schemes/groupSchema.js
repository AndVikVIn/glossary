const mongoose = require('mongoose');

const groupSchema = mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    name: String,
    creationDate: Date,
    wordsAmount: Number,
    userId: mongoose.Schema.Types.ObjectId
});

module.exports = groupSchema;
