const mongoose = require('mongoose');

const resultSchema = mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    userId: mongoose.Schema.Types.ObjectId,
    score: Number,
    correctUnswers: Number,
    incorrectUnswers: Number,
    testDate: Date
});

module.exports = resultSchema;
