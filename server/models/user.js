const mongoose = require('mongoose');
const userScheme = require('../schemes/userSchema');
const bcrypt = require('bcrypt');

const saltRounds = 10;

const User = mongoose.model('User', userScheme);

User.createNew = async (obj) => {
    const values = obj;
    const hashPass = await bcrypt.hash(obj.password, saltRounds);
    values.password = hashPass;
    const newUser = new User(values);
    newUser.id = newUser._id;
    let result = await newUser.save();
    return result;
};

User.checkEmail = async (obj) => {
    const result = await User.findOne({email: obj});
    return result;
};


module.exports = User;
