const mongoose = require('mongoose');
const resultSchema = require('../schemes/resultSchema');

const Result = mongoose.model('Result', resultSchema);

Result.addNew = async (values) => {
    const newResult = new Result(values);
    newResult.id = newResult._id;
    const result = await newResult.save();
    return result;
};

Result.getUsersResults = async (userId) => {
    const result = await Result.find({userId});
    return result;
};
module.exports = Result;
