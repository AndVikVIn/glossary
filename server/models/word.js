/* eslint-disable no-await-in-loop */
const mongoose = require('mongoose');
const wordSchema = require('../schemes/wordSchema');

const Word = mongoose.model('Word', wordSchema);

Word.createNew = async (values) => {
    const newWord = new Word(values);
    newWord.id = newWord._id;
    const result = await newWord.save();
    return result;
};

Word.getRandome = async (groupId) => {
    const randomeArr = [];
    const checkArr = [];
    const amountOfWords = await Word.aggregate(
        [
            {$match: {groupId: new mongoose.Types.ObjectId(groupId)}},
            {$count: 'groupMatch'}
        ]
    );
    for (let i = 0; i < 10 && i < amountOfWords[0].groupMatch; i++) {
        const arr = await Word.aggregate(
            [
                {$match: {groupId: new mongoose.Types.ObjectId(groupId)}},
                {$sample: {size: 1}}
            ]
        );
        const type = arr[0].type;
        const question = await Word.aggregate(
            [
                {$match: {groupId: new mongoose.Types.ObjectId(groupId), type}},
                {$sample: {size: 4}}
            ]
        );
        if (!checkArr.includes(question[0].name)) {
            randomeArr.push(question);
            checkArr.push(question[0].name);
        }
        else { i--; }
    }
    return randomeArr;
};

Word.getAll = async (groupId) => {
    const result = await Word.find({groupId});
    return result;
};

module.exports = Word;
