const mongoose = require('mongoose');
const groupSchema = require('../schemes/groupSchema');

const Group = mongoose.model('Group', groupSchema);

Group.createNew = async (values) => {
    const newGroup = new Group(values);
    newGroup.id = newGroup._id;
    const result = await newGroup.save();
    return result;
};

Group.changeAmount = async (id, amount) => {
    const group = await Group.findOne({_id: id});
    group.wordsAmount = amount;
    const result = await group.save();
    return result;
};

Group.getAll = async (userId) => {
    const result = await Group.find({userId});
    return result;
};

module.exports = Group;
