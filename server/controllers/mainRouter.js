const express = require('express');

const router = express.Router();

router.use(require('./userRoter'));

router.use(require('./groupRouter'));

router.use(require('./wordRouter'));

router.use(require('./resultRouter'));

module.exports = router;
