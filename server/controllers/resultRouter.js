const wrap = require('./wrap');
const express = require('express');

const router = express.Router();
const Result = require('../models/result');

const addNew = wrap(async (req, res) => {
    res.send(await Result.addNew(req.body));
});

const getUsersResults = wrap(async (req, res) => {
    res.send(await Result.getUsersResults(req.query.userId));
});

router.post('/addNewResult', addNew);
router.get('/getUsersResults', getUsersResults);

module.exports = router;
