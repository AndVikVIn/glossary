const wrap = require('./wrap');
const express = require('express');

const router = express.Router();
const Word = require('../models/word');

const createNew = wrap(async (req, res) => {
    res.send(await Word.createNew(req.body));
});

const getAll = wrap(async (req, res) => {
    res.send(await Word.getAll(req.query.groupId));
});

const getRandome = wrap(async (req, res) => {
    res.send(await Word.getRandome(req.query.groupId, req.query.type));
});

router.post('/createWord', createNew);
router.get('/words', getAll);
router.get('/getRandome', getRandome);

module.exports = router;
