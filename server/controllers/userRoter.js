const wrap = require('./wrap');
const express = require('express');

const router = express.Router();
const User = require('../models/user');
const passport = require('passport');
require('../passportStrategy');

const createNew = wrap(async (req, res) => {
    res.send(await User.createNew(req.body));
});

const checkEmail = wrap(async (req, res) => {
    res.send(await User.checkEmail(req.query.email));
});

const logout = wrap(async (req, res) => {
    req.session.destroy(() => {
        res.send({});
    });
});

const sendStatus = wrap(async (req, res) => {
    res.send(req.session.user || null);
});

const login = wrap(async (req, res, next) => {
    passport.authenticate('local', (err, user) => {
        if (err) return next(err);
        if (!user) res.send({});
        req.logIn(user, (error) => {
            if (error) return next(err);
            req.session.user = user;
            res.send(user);
            return false;
        });
        return false;
    })(req, res, next);
});

router.post('/login', login);
router.post('/logout', logout);
router.post('/login/status', sendStatus);
router.post('/newUser', createNew);
router.get('/checkEmail', checkEmail);

module.exports = router;
