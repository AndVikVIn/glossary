const wrap = require('./wrap');
const express = require('express');

const router = express.Router();
const Group = require('../models/group');

const createNew = wrap(async (req, res) => {
    res.send(await Group.createNew(req.body));
});

const getAll = wrap(async (req, res) => {
    res.send(await Group.getAll(req.query.userId));
});

const changeAmount = wrap(async (req, res) => {
    res.send(await Group.changeAmount(req.body.groupId, req.body.wordsAmount));
});

router.post('/changeGroup', changeAmount);
router.post('/createGroup', createNew);
router.get('/groups', getAll);

module.exports = router;
