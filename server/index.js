require('./dataBase');
const mongoose = require('mongoose');
const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const router = require('./controllers/mainRouter');

const app = express();
const MongoStore = require('connect-mongo')(session);
const passport = require('passport');


app.use(session({
    secret: 'zxczxcasdasdqweqwe2',
    resave: false,
    saveUninitialized: true,
    store: new MongoStore({mongooseConnection: mongoose.connection})
}));

app.use(passport.initialize());
app.use(passport.session());


function errorHandler(err, req, res) {
    res.status(500);
    res.render('error', {error: err});
}

app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: false}));

app.listen(3000);

app.use('/api', router);

app.use(errorHandler);

process.on('SIGINT', () => {
    mongoose.disconnect();
    process.exit();
});

